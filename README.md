README

This repository contains an app made by Avery Lamp with graphics by Ryan Sullivan for the High School Hackathon, HackTrin.  It is a spritekit game created in Objective-C.  It starts with history questions, then is a Street Fighter style game.  So far controlling the main character works, punching, jumping, walking, crouching.  The computer character can walk, but his attacking has not been implemented yet.  We still need to implement kicking and as well as adding the second sprite for the computer character, Ryu. 

The app uses sprites found online.  We will not be publishing the game in the future, due to copyright issues.

If you have any questions please contact me at averylamp@gmail.com