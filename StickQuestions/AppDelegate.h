//
//  AppDelegate.h
//  StickQuestions
//
//  Created by Avery Lamp on 1/27/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

