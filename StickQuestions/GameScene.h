//
//  GameScene.h
//  StickQuestions
//

//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameViewController.h"
@interface GameScene : SKScene
@property int damagePerHit;
@property GameViewController *gameController;
@end
