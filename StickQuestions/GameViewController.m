//
//  GameViewController.m
//  StickQuestions
//
//  Created by Avery Lamp on 1/27/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "QuestionScene.h"


@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.sceneIndex = 0;
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
    
    // Create and configure the scene.
     QuestionScene *scene = [QuestionScene unarchiveFromFile:@"QuestionScene"];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    scene.gameController = self;

    // Present the scene.
    [skView presentScene:scene];
    
        self.sceneIndex = 1;
}




- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}


-(void)loadNextScene{
    if(self.sceneIndex < 5){
        
        
        SKView * skView = (SKView *)self.view;
        skView.showsFPS = NO;
        skView.showsNodeCount = NO;
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = YES;
        
        // Create and configure the scene.
        QuestionScene *scene = [QuestionScene unarchiveFromFile:@"QuestionScene"];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        scene.gameController = self;
        
        // Present the scene.
        [skView presentScene:scene];
    }else{
        SKView * skView = (SKView *)self.view;
        skView.showsFPS = NO;
        skView.showsNodeCount = NO;
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = YES;
        
        // Create and configure the scene.
        GameScene *scene = [GameScene unarchiveFromFile:@"QuestionScene"];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        scene.gameController = self;
        
        // Present the scene.
        [skView presentScene:scene];
    }
    self.sceneIndex ++;
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
