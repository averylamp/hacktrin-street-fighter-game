//
//  GameViewController.h
//  StickQuestions
//

//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController
-(void)loadNextScene;
@property int sceneIndex;
@end
