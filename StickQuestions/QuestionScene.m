//
//  QuestionScene.m
//  StickQuestions
//
//  Created by Avery Lamp on 1/31/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import "QuestionScene.h"
#import "Question.h"

@interface QuestionScene()
@property SKLabelNode *questionLabel;
@property CGSize screenSize;
@property NSMutableArray *answerArray;
@property SKSpriteNode *question;
@end

@implementation QuestionScene


-(void) didMoveToView:(SKView *)view
{
    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    myLabel.text = @"Question Scene!";
    myLabel.fontSize = 65;
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame));
    //[self addChild:myLabel];
    
    self.screenSize = [UIScreen mainScreen].bounds.size;
    
    self.answerArray = [[NSMutableArray alloc] init];
    for(int i = 0; i < 4; i++){
        // [self.answerArray addObject:([[SKSpriteNode alloc] init])];
    }
    
    int questionSelector = arc4random() % 5 + 1;
    self.question = [[SKSpriteNode alloc] initWithImageNamed:[NSString stringWithFormat:@"%@%d%@", @"question", questionSelector, @"_question"]];
    self.question.name = [NSString stringWithFormat:@"%@%d%@", @"question", questionSelector, @"_question"];
    self.question.position = CGPointMake(self.frame.size.width/2 ,self.frame.size.height-200);
    [self addChild:self.question];
    //NSLog([NSString stringWithFormat:@"%@%d%@%d", @"question", questionSelector, @"_answer", 1]);
    for(int i = 1; i < 5; i++){
        [self.answerArray addObject:([SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"%@%d%@%d.png", @"question", questionSelector, @"_answer", i]])];
        if (i==1) {
            ((SKSpriteNode*)[self.answerArray objectAtIndex:0]).name = @"answer";
        }
        
    }
    
    
    for (int i=1;i<5; i++) {
        switch (i) {
            case 1:
                ((SKSpriteNode*) [self.answerArray objectAtIndex:(i-1)]).position = CGPointMake(300, self.frame.size.height / 2-200 );
                [self addChild: [self.answerArray objectAtIndex:(i-1)]];
                break;
            case 2:
                ((SKSpriteNode*) [self.answerArray objectAtIndex:(i-1)]).position = CGPointMake(300, self.frame.size.height / 2 );
                [self addChild: [self.answerArray objectAtIndex:(i-1)]];
                break;
            case 3:
                ((SKSpriteNode*) [self.answerArray objectAtIndex:(i-1)]).position = CGPointMake(self.frame.size.width -300, self.frame.size.height / 2 - 200);
                [self addChild: [self.answerArray objectAtIndex:(i-1)]];
                break;
            case 4:
                ((SKSpriteNode*) [self.answerArray objectAtIndex:(i-1)]).position = CGPointMake(self.frame.size.width -300, self.frame.size.height / 2);
                [self addChild: [self.answerArray objectAtIndex:(i-1)]];
                break;
                
            default:
                break;
        }
        
    }
    
    
    
    
}





-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint location = [[[touches allObjects]firstObject] locationInNode:self];
    SKSpriteNode *touchedNode = (SKSpriteNode*)[self nodeAtPoint:location];
    SKLabelNode* correct;
    if ([touchedNode.name isEqualToString:@"answer"]) {
        correct = [SKLabelNode labelNodeWithText:@"Correct!"];
        correct.fontColor = [UIColor greenColor];
    }else{
        correct = [SKLabelNode labelNodeWithText:@"Incorrect"];
        correct.fontColor = [UIColor redColor];
    }
    
    correct.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    correct.fontSize = 140.0f;
    
    correct.zPosition = 1;
    correct.fontName = @"Impact";
    [self addChild:correct];
    
    
    dispatch_queue_t queue = dispatch_queue_create("wait", NULL);
    dispatch_async(queue, ^{
        int wait=5;
        double endTime = CACurrentMediaTime() + wait;
        while (CACurrentMediaTime() <endTime) {
            
        }
        
        [self.gameController loadNextScene];
        
        
        
    });
    
    
    
    
}





@end
