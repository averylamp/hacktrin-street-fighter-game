//
//  Question.h
//  StickQuestions
//
//  Created by Avery Lamp on 1/31/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

@property (strong)NSString *    question;
@property (strong)NSMutableArray *answers;
@end
