//
//  QuestionScene.h
//  StickQuestions
//
//  Created by Avery Lamp on 1/31/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameViewController.h"
@interface QuestionScene : SKScene
@property GameViewController *gameController;
@end
